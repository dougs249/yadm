# fzf
  - Start `fzf` on all files in the specified directory:
    find path/to/directory -type f | fzf

  - Start `fzf` for running processes:
    ps aux | fzf

  - Select multiple files with `Shift + Tab` and write to a file:
    find path/to/directory -type f | fzf --multi > path/to/file

  - Start `fzf` with a specified query:
    fzf --query "query"

  - Start `fzf` on entries that start with core and end with either go, rb, or py:
    fzf --query "^core go$ | rb$ | py$"

  - Start `fzf` on entries that not match pyc and match exactly travis:
    fzf --query "!pyc 'travis"
  
#############
# Learning
#############
##  Pipe fzf file search selection into nvim
  `nvim $(fzf)`

  `nvim $(fd . --hidden --max-depth 2 | fzf --preview "bat --color=always --style=numbers --line-range=:500 {}" --height=80% --layout=reverse --preview-window=right,70%)`

####  A more robust solution would be to use xargs but we've presented the above as it's easier to grasp
  `fzf --print0 | xargs -0 -o vim`

####  NOTE
fzf also has the ability to turn itself into a different process.

