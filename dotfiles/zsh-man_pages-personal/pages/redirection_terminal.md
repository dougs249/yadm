command | tee output.txt
ls -l | tee file_list.txt
command 2>&1 | tee output.txt
command | tee -a output.txt
You can use the `tee` command to achieve this. Here's how it works:

```bash
command | tee output.txt

This will:
* **Run the `command`:**  Execute your desired command-line program.
* **Pipe the output:** Send the standard output (STDOUT) of the command to the `tee` command.
* **Display and save:** `tee` will both display the output on your terminal (STDOUT) and save a copy of it in `output.txt`.

**Example:**

```bash
ls -l | tee file_list.txt

This will list all files and directories in the current directory, display the list on your terminal, and save the same list in a file named `file_list.txt`.

**Including Standard Error (STDERR)**
If you want to capture both STDOUT and STDERR in the file, you can redirect STDERR to STDOUT before piping to `tee`:

```bash
command 2>&1 | tee output.txt
```
**Appending to the file**
To append the output to an existing file instead of overwriting it, use the `-a` option with `tee`:

```bash
command | tee -a output.txt 

