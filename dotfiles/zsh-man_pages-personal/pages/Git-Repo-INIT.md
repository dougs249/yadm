---
aliases: []
tags: []
title: Git-Repo-INIT
---

# Git-Repo-INIT
call like any man page
```
man me <comand>
```

## To do
- man me help
    - for list of all available commands w/ help
- include custom functions and stuff too

---
- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

---
```
cd existing_repo
git remote add origin https://gitlab.com/dougs249/man.git
git branch -M main
git push -uf origin main
```
