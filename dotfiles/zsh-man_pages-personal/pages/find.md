# find
- Find files by extension:

`find {{root_path}} -name '{{*.ext}}'`

- Find files matching multiple path/name patterns:

`find {{root_path}} -path '{{**/path/**/*.ext}}' -or -name '{{*pattern*}}'`

- Find directories matching a given name, in case-insensitive mode:

`find {{root_path}} -type d -iname '{{*lib*}}'`

- Find files matching a given pattern, excluding specific paths:

`find {{root_path}} -name '{{*.py}}' -not -path '{{*/site-packages/*}}'`

- Find files matching a given size range, limiting the recursive depth to "1":

`find {{root_path}} -maxdepth 1 -size {{+500k}} -size {{-10M}}`

- Run a command for each file (use `{}` within the command to access the filename):

`find {{root_path}} -name '{{*.ext}}' -exec {{wc -l}} {} \;`

- Find all files modified today and pass the results to a single command as arguments:

`find {{root_path}} -daystart -mtime {{-1}} -exec {{tar -cvf archive.tar}} {} \+`

- Find empty files (0 byte) or directories and delete them verbosely:

`find {{root_path}} -type {{f|d}} -empty -delete -print`

## Examples
- Using find with move to move files

`
find [DIR to search] -type f[doc type] {-name ###} -print0 {format for xargs} | xargs -0 /bin/mv -f --target-directory=[DIR]
`
```
find /mnt/Large-Files/Porn/01-shota -type f -print0 | xargs -0 /bin/mv -f --target-directory=/mnt/Large-Files/Porn/01-shota/
```
```
find /mnt/Large-Files/Porn/01-shota -type f -print0 | xargs -0 /bin/mv -f --target-directory=/mnt/Large-Files/Porn/01-shota/0000
```

```
find ./000-sort -type f -print0 | xargs -0 /bin/mv -f --target-directory=/mnt/Large-Files/Porn/01-shota/0000
```

```
find . -type f -printf '%f\n'
```

○ find . -type f -printf '[%f]\n' | echo
find . -type f -printf '[%f]\n' | echo '{%f}'
find . -type f -printf '[%f]\n' | echo %f
find . -type f -printf '[%h][%f]\n'
find . -type f -exec file '{}' \;

man xargs

find * -fprintf %h
find * -print0
find * -fls .file-list
