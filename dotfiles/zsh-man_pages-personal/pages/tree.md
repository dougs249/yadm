# fzf-dir-navigator using tree



Below are the set of default keybindings and the actions they perform:
Default Keybinding 	Action
Ctrlf 	When in the $HOME folder, it brings up the global search window on the terminal and the directory history. If you are inside a different folder, it searches folders inside the current folder. Upon pressing Ctrlf again, it brings back the global search window

```
Ctrlv 	Brings back the current directory search window on the terminal if you are searching globally.
Ctrlr 	Resets the recent directory history and brings up the global search window on the terminal.
Alt← 	Goes to the previous working directory. (similar to prevd in fish)
Alt→ 	Goes to the next working directory. (similar to nextd in fish)
```

## Config
There is a default config file where some values can be tweaked. To make your own config file, please copy this file and rename it as fzf-dir-navigator-custom.conf. The plugin will automatically use this config file or fall back to the default config file.

```
cp /path/to/cloned/repo/fzf-dir-navigator.conf /path/to/cloned/repo/fzf-dir-navigator-custom.conf
```
```
Option 	Description 	Default
dir_histsize 	Number of recent directories to store and display. 	10
history_file 	File to store the history. 	$HOME/.local/share/zsh/widgets/fzf-dir-navigator-history
exclusions 	The directories to exclude while using this widget. 	`( ".git" "node_modules" ".venv" "__pycache__" ".vscode" ".cache" )`
search_home 	Keybinding used to search the $HOME directory. If you are changing the search_home keybinding, please be sure to add it to your .zshrc file as well. For example, if you are changing it to ctrl-p, add bindkey "^P" fzf-dir-navigator to your .zshrc after sourcing the plugin. Otherwise the keybinding to open the plugin on the terminal would still remain ctrl-f, and ctrl-p would work only after the plugin is open. 	ctrl-f
search_pwd 	Keybinding used to search the $PWD directory. 	ctrl-v
reset_history 	Keybinding used to reset the directory history. 	ctrl-r
os 	To set which OS is being used. 	The default assumes Linux and does not need a value. If you have a Mac, set os="mac".

```

