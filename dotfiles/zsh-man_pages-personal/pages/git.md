# My git and YADM aliases
```
gits="git status"
git-push='git push --progress '
gitup='git pull -ff; git add *;git add .*; git stage --all; git commit -a -m "Auto Sync-- $NOW"; git push --progress'
```
```
yadms='yadm status'
yadml='yadm list'
yadmup='sh /home/doug/Code/shell-scripts/yadm-git/yadm-auto-sync.zsh'
yadma='yadm add '
```
# Git-Repo-INIT
`
cd <$DIR>
`

`
git remote add origin https://gitlab.com/dougs249/<$REPO>
`
```
git branch -M main
git push -uf origin main
```
`
git clone git@gitlab.com:dougs249/<$REPO>
`

`
cd <$DIR[Repo-name]>
`
```
git switch --create main
touch README.md
git add README.md
git commit -m "add README"
git push --set-upstream origin main
```

### Push an existing folder
```
cd existing_folder
git init --initial-branch=main
git remote add origin git@gitlab.com:dougs249/zsh.git
git add .
git commit -m "Initial commit"
git push --set-upstream origin main
```
### Push an existing Git repository
```
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:dougs249/zsh.git
git push --set-upstream origin --all
git push --set-upstream origin --tags
```

# Branch Management
## Merge BRANCH into Main
```
git checkout <default-branch>
git merge <feature-branch>
```
## Feature branch workflow
`
cd <$DIR[branch]>
`

Create a new branch

`
git checkout -b <new-branch-name>
`

Work on branch
```
git add 
git commit -am "...."
git push origin <new-feature-name>
```
- Then you can MERGE with the previous CLI steps

## Git Aliases
AKA: Aliases for Just git
#### How to make
`
git config --global alias.mwps "push -o merge_request.create -o merge_request.target=main -o merge_request.merge_when_pipeline_succeeds
`

`
git mwps origin <local-branch-name>
`

### To do
- maan help
    - for list of all available commands w/ help
- include custom functions and stuff too

---
- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

---

