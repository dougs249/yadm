# xargs

> Execute a command with piped arguments coming from another command, a file, etc.
> The input is treated as a single block of text and split into separate pieces on spaces, tabs, newlines and end-of-file.
> More information: <https://pubs.opengroup.org/onlinepubs/9699919799/utilities/xargs.html>.

- Run a command using the input data as arguments:

`{{arguments_source}} | xargs {{command}}`

- Run multiple chained commands on the input data:

`{{arguments_source}} | xargs sh -c "{{command1}} && {{command2}} | {{command3}}"`

- Gzip all files with `.log` extension taking advantage of multiple threads (`-print0` uses a null character to split file names, and `-0` uses it as delimiter):

`find . -name '*.log' -print0 | xargs -0 -P {{4}} -n 1 gzip`

- Execute the command once per argument:

`{{arguments_source}} | xargs -n1 {{command}}`

- Execute the command once for each input line, replacing any occurrences of the placeholder (here marked as `_`) with the input line:

`{{arguments_source}} | xargs -I _ {{command}} _ {{optional_extra_arguments}}`
find ~/ -name words.txt -print0 | xargs -0 -I

- Parallel runs of up to `max-procs` processes at a time; the default is 1. If `max-procs` is 0, xargs will run as many processes as possible at a time:

`{{arguments_source}} | xargs -P {{max-procs}} {{command}}`

## Flags / options
-a                  input file instead of piping into xargs
    ` xargs -0 -p -a {{input_file}} /bin/mv -f --target-directory={{Directory}}`

-0                  use null character toseperate (like input from -print0)

-p                  --interactive, print the comand that will run before running it (interactive)

-t, --verbose       Print the command line on the standard error output before executing it.

-I replace-str
Replace occurrences of replace-str in the initial-arguments with names read from standard input.  Also, unquoted blanks
do not terminate input items; instead the separator is the newline character.  Implies -x and -L 1.

-i[replace-str], --replace[=replace-str]    Synonym for -Ireplace-str if replace-str is specified.  If the replace-str argument  is  missing,  the
effect is the same as -I{}.  The -i option is deprecated; use -I instead.\

-d []               Input items deliminated [] character. may be a single  character, C-style escape such as \n, etc  

### EX
xargs -0 -p -a dirs


### History with them learning 
find . -print0 | xargs -0 mv -t ./0000/\n

fd . | xargs -0 mv -t \n
fd . | xargs -0 mv -t ./0000/\n

find . -print0 | xargs -0 mv -t 0000/\n

cat /home/doug/Code/filestodelete | xargs -d "\n" rm
cat aFile | xargs -d "\n" rm
cat filesToDelete | xargs -d "\n" rm

find /mnt/Large-Files/Porn/01-shota -type f -print0 | xargs -0 /bin/mv -f --target-directory=/mnt/Large-Files/Porn/01-shota/
find /mnt/Large-Files/Porn/01-shota -type f -print0 | xargs -0 /bin/mv -f --target-directory=/mnt/Large-Files/Porn/01-shota/0000

find ./000-sort -type f -print0 | xargs -0 -n1 /bin/mv -f --target-directory=/mnt/Large-Files/Porn/01-shota/0000
find ./ -type f -print0 | xargs -0 /bin/mv -f --target-directory=/mnt/Large-Files/Porn/01-shota/0000
find ./ -type f -print0 | xargs -0 /bin/cp -f --target-directory=/mnt/Large-Files/Porn/01-shota/0000

find ./ -name test -print0 | xargs -0 /bin/cp -f --target-directory=/home/doug

find ./ -name test -print0 | xargs -0 /bin/cp -f
find ./ -name test -print0 | xargs -0 /bin/rm -f
find ./ -name test -print | xargs /bin/rm -f

find ./ -name test -print0 | xargs -0 /bin/rm

```
xargs -0 -p -a .mv /bin/mv {}
xargs -0 -p -a .mv /bin/mv "{}" ~/
xargs -0 -p -a .mv /bin/mv ~/
xargs -0 -p -a .mv /bin/mv
xargs -0 -p -a .mv /bin/sort
```


