# My custom or used keybinds

'ctrl + r' --- history fzf
'ctrl + f' --- fd with fzf
'ctrl + t' --- Delete leading copied $prompt
'ctrl + Z' --- Suspend and resume vim