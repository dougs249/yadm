# Markdown Cheat Sheet
## Basic Syntax
**bold text**
*italicized text*
> blockquote

### Ordered List
1. First item
2. Second item
3. Third item

### Unordered List

- First item
- Second item
- Third item

### Code

`code`

### Link

[Markdown Guide](https://www.markdownguide.org)

### Image

![alt text](https://www.markdownguide.org/assets/images/tux.png)

## Extended Syntax

### Table

| Syntax | Description |
| ----------- | ----------- |
| Header | Title |
| Paragraph | Text |

### Fenced Code Block

```
{
  "firstName": "John",
  "lastName": "Smith",
  "age": 25
}
```

### Footnote

Here's a sentence with a footnote. [^1]

[^1]: This is the footnote.

### Heading ID

### My Great Heading {#custom-id}

### Definition List

term
: definition

### Strikethrough

~~The world is flat.~~

### Task List

- [x] Write the press release
- [ ] Update the website
- [ ] Contact the media


### Highlight

I need to highlight these ==very important words==.

### Subscript

H~2~O

### Superscript

X^2^
asic navigation and actions remain available regardless of keyboard shortcuts being active or not in the settings.
Basic navigation


# Proton Mail Keybinds
    Move up
    ↑
    Jump to first
    Ctrl + ↑
    Move down
    ↓
    Jump to last
    Ctrl + ↓
    Move right / expand
    →
    Move left / collapse
    ←

Basic actions

    Apply / open
    Enter
    Cancel / close
    Escape
    Open this modal
    ?
    Select / unselect
    Space
    Open command panel
    Ctrl + K

Keyboard shortcuts
Folder shortcuts

    Go to InboxGI
    Go to ArchiveGA
    Go to SentGE
    Go to StarredG*
    Go to DraftsGD
    Go to TrashGT
    Go to SpamGS
    Go to All MailGM

Composer shortcuts

    Save draft
    Ctrl + S
    Send email
    Ctrl + Enter
    Close draft
    Escape
    Minimize / maximize composer
    Ctrl + M
    Expand / contract composer
    Ctrl + Shift + M
    Attach file
    Ctrl + Shift + A
    Add expiration time
    Ctrl + Shift + X
    Add encryption
    Ctrl + Shift + E
    Insert link
    Ctrl + K
    Discard draft
    Ctrl + Alt + Backspace

List shortcuts

    Open previous message
    K
    Open next message
    J
    Select / unselect
    X
    Show unread emails
    Shift + U
    Show all emails
    Shift + A
    Select / unselect all
    Ctrl + A
    Search
    /

Action shortcuts

    New message
    N
    Star
    *
    Mark as unread
    U
    Mark as read
    R
    Label as...
    L
    Filter
    F
    Move to...
    M
    Move to Inbox
    I
    Move to Archive
    A
    Move to Spam
    S
    Move to Trash
    T
    Delete permanently
    Ctrl + Backspace

Message shortcuts

    Reply
    R
    Reply all
    Shift + R
    Forward
    Shift + F
    Load remote content
    Shift + C
    Load embedded images
    Shift + E
    Show original message
    O