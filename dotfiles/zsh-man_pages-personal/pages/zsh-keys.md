

## General shortcuts

- Ctrl+r
  fzf Hist search

## Command line editing
- Ctrl+u
  Delete line to beginning
