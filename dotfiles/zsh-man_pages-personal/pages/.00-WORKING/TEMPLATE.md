---
aliases: []
tags: []
title: systemctl
---

# systemctl
How to use

## a
- placeholder
`
sudo systemctl daemon-reload
`

## systemctl --user
- use for managing systemd timers / units
  - located in ~/.config/systemd/user/

```
systemctl --user start ~/.config/systemd/user/helloworld.timer
systemctl --user enable ~/.config/systemd/user/helloworld.timer
systemctl --user list-timers
journalctl --user -u helloworld.*
systemd-analyze verify ~/.config/systemd/user/helloworld.timer
```

[Reference](https://documentation.suse.com/sle-micro/6.0/html/Micro-systemd-working-with-timers/index.html)