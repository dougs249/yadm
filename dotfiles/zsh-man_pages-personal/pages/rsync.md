# rsync

## Copy SOURCE contents into DEST 

`
rsync -avP --inplace  path/to/Source/    path/to/Dest
`

## Copy SOURCE  entire folder into DEST
IE  make a **NEW** folder **WITHIN** the DEST

`
rsync -avP --inplace   path/to/Source    path/to/Dest
`

## In Summary
- Trailing ' / ' means copy  **the contents of [folder]**
  - IE  put all the stuff INSIDE of ~/music/ into the root of ~/photos
- leaving the trailing ' / ' off would
  - put a folder called ~/music into ~/photos


# LAN copying
`
$rsync -avP --inplace [src] [dest]
`

```bash
rsync -avP --inplace  /path/to/source /path/to/dest
```

# Network copying

`
$rsync -avz [src] [dest]
`

[Github Reference](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)




