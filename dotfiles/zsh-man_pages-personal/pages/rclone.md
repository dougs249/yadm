# rclone

## Basic commands
```
rclone config reconnect google\ photos:
rclone config reconnect protondrive:
rclone config reconnect protondrive

rclone listremotes
rclone lsf protondrive:
rclone lsd protondrive:

rclone authorize protondrive:
rclone authorize -h
rclone authorize
```
## Template
```
alias rclone-help-tip='command rclone sync \
--progress --progress-terminal-title -vv --refresh-times  --track-renames \
SOURCE \
 DEST:FOLDER'
```
# Example command
- Picture backup to Proton Drive
```
rclone sync --progress --progress-terminal-title -vv --refresh-times --track-renames /home/doug/Pictures/01-Jet protondrive:Photos/01-Jet/
```
- Written better with minimal flags
```
alias rclone-help-tip='command rclone sync --progress -vv  /home/doug/Pictures/01-Jet protondrive:Photos/01-Jet/
```
## add SUFFIX instead of deleting a file
```
rclone sync [--interactive] /path/to/local/file remote:current --suffix .bak --exclude "*.bak"
```
## Flags
```
track-renames 
transfers=N 
update 
progress
progress-terminal-title 
vv 
refresh-times 

--suffix [.bak] 
--exclude ["*.bak"]
```
---
---
See more at: 
- https://rclone.org/docs/

- picture backup script
`
/home/doug/Code/shell-scripts/rsync-rclone/Picture-Backup-Proton_Drive-Rclone.zsh
`
- rclone's default man pages in MD
`
/home/doug/dotfiles/zsh-man_pages-personal/pages/rclone
`


