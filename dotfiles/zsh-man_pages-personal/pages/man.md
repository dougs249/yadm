.TH man-me 1
.SH EXAMPLES
location of files:
.PP
.nf
.Rs
$HOME/man/manme
.RE
.fi
.PP
Online help:
.PP
.nf
.Rs
https://liw.fi/manpages/
.PP
.Re
.fi
