## fd

### TLDR documentation

Find files that begin with `foo`:  
`fd "^foo"`

Find files with a specific extension:`fd --extension txt`

Include ignored and hidden files in the search:  
`fd --hidden --no-ignore "{{string|regex}}"`

Execute a command on each search result returned:`fd "{{string|regex}}" --exec {{command}}`

## flags

type `-t[f], --type [filetype]`

```plaintext
    directory       d, dir, directory
    sym-link        l, symlink
    file            f, file
    empty           e, empty
    executable      x, executable
```

Extension `-e, --extension [ext]`

Exclude `-E, --exclude pattern`

format `--format [fmt]`

*   The following placeholders are substituted into the string for each file before printing

{/} basename

{//} parent directory

{.} path without file extension

{/.} basename without file extension

{{ literal '{' (an escape sequence)

}} literal '}' (an escape sequence)

Notice that you can use "{{" and "}}" to escape "{" and "}" respectively, which is especially useful if you need to in‐clude the literal text of one of the above placeholders

```plaintext
Search Directory `--search-path [search-path]`
- Provide  paths  to  search  as an alternative to the positional path argument. Changes the usage to `fd [FLAGS/OPTIONS]` 
```

fd -tf --format {/} --search-path /path/to/source

```plaintext
Base{Working} Directory `--base-directory path`
- Change the current working directory of fd to the provided path. This means that search results will be shown with  respect  to  the given base path. Note that relative paths which are passed to fd via the positional path argument or the
- The `--search-path` option will also be resolved relative to this directory.
- 
#### Examples 
- Find empty files:
```

fd --type empty --type file  
fd -te -tf

```plaintext

- Find empty directories:
```

fd --type empty --type directory  
fd -te -td

```plaintext

- Find both files and symlinks
fd --type file --type symlink …
fd -tf -tl

### History with them learning 
fd -tf --format {/} --search-path /home/doug/dotfiles/zsh-man_pages-personal/pages
```

fd -tf --search-path /home/doug/dotfiles/zsh-man\_pages-personal/pages -x echo {} {.} {/} {//} {/.}

fd -tf --format {/.} --search-path /home/doug/dotfiles/zsh-man\_pages-personal/pages

fd -tf --extension md --format {/} --search-path /home/doug/dotfiles/zsh-man\_pages-personal/pages

```plaintext

fd -tf --search-path /mnt/Large-Files/Porn/03-furry -x mv {} /mnt/Large-Files/Porn/03-furry/.00-sort{/}


```

```plaintext
{}     path (of the current search result)
```

fd --extension bps -x flips -a {} /mnt/USB/Games/00-rom-hacks/smw/patch-files/supermarioworld-flips\_target.smc {.}.smc