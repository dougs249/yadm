Contains script to 
- move or synlink contined dotfiles / configs to appropriate locations
- clone appropriate other github repos 
  - nvim, work, hass, etc
- install needed extra packages and system update
- cron setup for above
  - and timeshift, git-update, etc
- setup custom man pages

- see the to-do.md text

---
### git 
```
cd existing_repo
git remote add origin https://gitlab.com/new8552201/test.git
git branch -M main
git push -uf origin main
```
