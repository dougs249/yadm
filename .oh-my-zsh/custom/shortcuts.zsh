# 
#  ***********    ROM file stuffs   ***********     
alias -g smwrom='/home/doug/Games/00-rom-hacks/smw/patch-files/supermarioworld.smc'
# alias -g romhacksd='/home/doug/Games/00-rom-hacks/smw/roms_patched/00-new_sort-and-add-to-snes-games-folder'
# **********     Directory Shortcuts and File edit  *********** 

alias cdzsh="cd $HOME/.oh-my-zsh/custom/"
alias cdman="cd $HOME/obsidian/"
alias zshrc="nvim /home/doug/.zshrc; exec zsh"