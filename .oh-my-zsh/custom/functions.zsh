# 
# ======================
# flips patch all () 
# ======================
function my-flips (){
  fd --extension bps -x flips -a {}  /mnt/USB/Games/00-rom-hacks/smw/patch-files/supermarioworld-flips_target.smc {.}.smc
}
    # also make one to unzip/uncompress all THEN flips then move all .smc to 'roms to move folder' 
    ### also make it do ips and bps
# ======================
# shell command line to alias quick fxn 
# ======================
function AA () {
	echo "alias ${1}='${2}'" >> /home/doug/.zshrc && exec zsh
}
# ======================
# renaming functions
# ======================
function my-rename () {
        if [ -z $1 ];then echo Give target directory; exit 0;fi

        find "$1" -depth -name '*' | while read file ; do
                directory=$(dirname "$file")
                oldfilename=$(basename "$file")
                newfilename=$(echo "$oldfilename" | tr ' ' '_' | sed 's/_-_/-/g' | sed 's/:/-/g' | sed 's/,//g' | sed 's/?//g' | sed "s/'//g" | sed 's/#-//g' | sed 's/#--//g' |  sed 's/[^ ]\+/\L\u&/g')
                #  | sed 'y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/')
                if [ "$oldfilename" != "$newfilename" ]; then
                        mv -i "$directory/$oldfilename" "$directory/$newfilename"
                        echo "  "$directory/$oldfilename" ---> "$directory/$newfilename""
                        #echo "$directory"
                        #echo "$oldfilename"
                        #echo "$newfilename"
                        #echo
                else
                        echo "$file"
                fi
                done
        # exit 0
}
function my-lowercase () {
        if [ -z $1 ];then echo Give target directory; exit 0;fi

        find "$1" -depth -name '*' | while read file ; do
                directory=$(dirname "$file")
                oldfilename=$(basename "$file")
                newfilename=$(echo "$oldfilename" | tr -d "[:upper:]" tr "[:lower:]" )
                if [ "$oldfilename" != "$newfilename" ]; then
                        mv -i "$directory/$oldfilename" "$directory/$newfilename"
                        echo ""$directory/$oldfilename" ---> "$directory/$newfilename""
                        #echo "$directory"
                        #echo "$oldfilename"
                        #echo "$newfilename"
                        #echo
                fi
                done
        # exit 0
}
# ======================
##  Man pages stuff \\ Select file for neovim  \\  neovim man page directly
# ======================
function zz () {
    glow --pager /home/doug/dotfiles/zsh-man_pages-personal/pages/$(fd --search-path /home/doug/dotfiles/zsh-man_pages-personal/pages --format {/.} --exclude ".rclone" | fzf --preview "bat --color=always --line-range=:500 /home/doug/dotfiles/zsh-man_pages-personal/pages/{}.md" --height=80% --layout=reverse --preview-window=right,70%).md
}
#function maan () {
#    glow /home/doug/dotfiles/zsh-man_pages-personal/pages/$(fd --search-path /home/doug/dotfiles/zsh-man_pages-personal/pages --format {/.} --exclude ".rclone" | fzf --preview "bat --color=always --line-range=:500 /home/doug/dotfiles/zsh-man_pages-personal/pages/{}.md" --height=80% --layout=reverse --preview-window=right,70%).md
#}
##  A more robust solution would be to use xargs but we've presented the above as it's easier to grasp
# fzf --print0 | xargs -0 -o vim
# function vims () {
#     nvim $(fd . --hidden --max-depth 2 | fzf --preview "bat --color=always --line-range=:500 {}" --height=80% --layout=reverse --preview-window=right,70%)
# }
# function vimman () {
#     nvim /home/doug/dotfiles/zsh-man_pages-personal/pages/$(fd --search-path /home/doug/dotfiles/zsh-man_pages-personal/pages -tf --format {/.} --exclude ".rclone" | fzf --preview "bat --color=always --line-range=:500 /home/doug/dotfiles/zsh-man_pages-personal/pages/{}.md" --height=80% --layout=reverse --preview-window=right,70%).md
# }
# ======================
#  make INTO new dir
# ======================
function mkcd () {
	mkdir -p ${1}
	cd ${1}
}
# ======================
#  7zip and eztract ( ?? add other formats IE tar, etc )
# ======================
my-zip-extract-all () {
  for zipfile in *.zip; do
    filename="${zipfile%.*}"     # Extract the filename without the .zip extension
    mkdir -p "$filename"     # Create a directory with the filename
    unzip -q "$zipfile" -d "$filename"     # Unzip the archive into the created directory
    rm "$zipfile"     # Delete the original zip archive
  done
}
# ======================
#   package management    
#     pacman and yay    
# ======================
##  generate list of all packages (From synced info). Use fzf to parse this and select to install w/ YAY
function y-s () {
  yay -Syu $(yay -Slq | fzf -m --preview 'yay -Si {}' --height=90% --layout=reverse --preview-window=right,70%)
}
##  Just list all packages to preview -Qi and the -Ql list of files
##  Use flag  `-e`  to parse only Explicit packages or `-dtq` for only dependencies
function y-installed () {
  yay -Q ${1} | awk '{print $1}' '-' | fzf -m --preview 'yay -Qil {}' --height=90% --layout=reverse --preview-window=right,70% --bind 'enter:execute(pacman -Qil {} | bat)'
}
##  generate list of all installed packages. Use fzf to parse them and select for removal
##  Use flag  `-e`  to parse only Explicit packages or `-dtq` for only dependencies
function y-r () {
  yay -Q ${1} | awk '{print $1}' '-' | fzf -m --preview 'yay -Qil {}' --height=90% --layout=reverse --preview-window=right,70% --bind 'enter:execute(yay -Rsn {} | bat)'
}
## generate list of dependencies not needed to parse w/ fzf for removal
function y-orphaned () {
  yay -Rsn $(yay -Qdtq | awk '{print $1}' '-' | fzf -m --preview 'yay -Qil {}' --height=90% --layout=reverse --preview-window=right,70%)
}
# ======================
#   ???
# ======================
