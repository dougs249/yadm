```rust
use std::process::Command;

fn main() {
    // Specify the URL of the webpage you want to open
    let url = "https://www.example.com";

    // Open the URL in the default web browser
    match open::that(url) {
        Ok(_) => println!("Opened {} in your default browser", url),
        Err(e) => println!("Error opening {}: {}", url, e),
    }
}
```

**Explanation:**

* **`use std::process::Command;`**: This line imports the `Command` struct from the `std::process` module, which is used to execute external commands.
* **`let url = "https://www.example.com";`**: This line defines a variable `url` and assigns it the URL of the webpage you want to open. Make sure to replace this with your desired URL.
* **`open::that(url)`**: This line uses the `open` crate to open the specified URL in the default web browser of the user's operating system.
* **`match ...`**: This is a `match` expression that handles the result of `open::that(url)`.
    * **`Ok(_)`**: If the URL was opened successfully, this branch will print a success message to the console.
    * **`Err(e)`**: If there was an error opening the URL, this branch will print an error message to the console, including the error details.

**To run this code:**

1. **Save the code:** Save the code as a Rust file (e.g., `open_webpage.rs`).
2. **Install the `open` crate:** Open your terminal and run the following command to add the `open` crate to your project's dependencies:

   ```bash
   cargo add open
   ```

3. **Build and run:** Run the following command in your terminal to build and run the program:

   ```bash
   cargo run
   ```

This will open the specified webpage in your default web browser.

**Note:** This program simply opens the webpage in the browser. If you want to create a more complex application that displays a webpage within its own window, you would need to use a GUI toolkit like `gtk`, `qt`, or `webview`.
