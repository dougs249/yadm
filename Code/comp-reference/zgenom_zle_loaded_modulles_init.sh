# {{{
[[ -z $ZGENOM_PLUGINS ]] || return 0

export PMSPEC=0fiPs
export ZPFX=/home/doug/.zgenom/polaris

ZGENOM_PLUGINS=(ohmyzsh/ohmyzsh/___ ohmyzsh/ohmyzsh/___ ohmyzsh/ohmyzsh/___ joshskidmore/zsh-fzf-history-search/___ zshzoo/magic-enter/___ wfxr/formarks/___ pabloariasal/zfm/___ romkatv/powerlevel10k/___ djui/alias-tips/___ zsh-users/zsh-autosuggestions/___ Freed-Wu/zsh-colorize-functions/___ KulkarniKaustubh/fzf-dir-navigator/___ zdharma-continuum/fast-syntax-highlighting/___ wfxr/forgit/___ aoyama-val/zsh-delete-prompt/___ elstgav/branch-manager/___ ohmyzsh/ohmyzsh/___ ohmyzsh/ohmyzsh/___ ohmyzsh/ohmyzsh/___ ohmyzsh/ohmyzsh/___ ohmyzsh/ohmyzsh/___)

ZSH=/home/doug/.oh-my-zsh

# ### Plugins & Completions
fpath=(/home/doug/.zgenom/sources/ohmyzsh/ohmyzsh/___/plugins/copybuffer ${fpath})

# ### General modules
typeset -ga zsh_loaded_plugins
zsh_loaded_plugins+=( "ohmyzsh/ohmyzsh/plugins/copybuffer" )
set -- && ZERO="/home/doug/.zgenom/sources/ohmyzsh/ohmyzsh/___/plugins/copybuffer/copybuffer.plugin.zsh" source "/home/doug/.zgenom/sources/ohmyzsh/ohmyzsh/___/plugins/copybuffer/copybuffer.plugin.zsh"

# }}}
#
# NEW maybe bad below
#
# {{{
[[ -z $ZGENOM_PLUGINS ]] || return 0

export PMSPEC=0fiPs
export ZPFX=/home/doug/.zgenom/polaris

ZGENOM_PLUGINS=(ohmyzsh/ohmyzsh/___ joshskidmore/zsh-fzf-history-search/___ zshzoo/magic-enter/___ wfxr/formarks/___pabloariasal/zfm/___ romkatv/powerlevel10k/___ djui/alias-tips/___ zsh-users/zsh-autosuggestions/___ Freed-Wu/zsh-colorize-functions/___ KulkarniKaustubh/fzf-dir-navigator/___ zdharma-continuum/fast-syntax-highlighting/___ wfxr/forgit/___ aoyama-val/zsh-delete-prompt/___ elstgav/branch-manager/___ ohmyzsh/ohmyzsh/___ ohmyzsh/ohmyzsh/___ ohmyzsh/ohmyzsh/___ ohmyzsh/ohmyzsh/___ ohmyzsh/ohmyzsh/___)

ZSH=/home/doug/.oh-my-zsh

# }}}