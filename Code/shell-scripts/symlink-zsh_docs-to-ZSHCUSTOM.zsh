#!/bin/zsh

################################################################################
#                                                                              #
#                                                                              #
#                                 file list                                    #
#                                                                              #
#                                                                              #
################################################################################
#  Call to make the file with the list of all zsh files in dotfile dir
##      Can move to its own file and remove function from BOTH this and the filelinker if BROKEN
setup-zsh-list-files () {
    fd --hidden --fixed-strings --extension zsh . /home/doug/dotfiles/zsh | cat $1 > $HOME/dotfiles/zsh/.list-of-zsh-files.temp
}

################################################################################
#                                                                              #
#                                                                              #
#                                file linker                                   #
#                                                                              #
#                                                                              #
################################################################################
#  Call this with the above .list-of-zsh-files.temp as argument
##    $>  ./symlink-zsh_docs-to-ZSHCUSTOM.zsh $HOME/dotfiles/zsh/.list-of-zsh-files.temp
setup-zsh-symlink () {
    filename="$1"
    # echo "${filename}"   ## DEBUG
    while IFS= read -r line; do
        # echo "${line}" 
        dirn=${line}
        # echo "${dirn}"
        ln -s "${dirn}" /home/doug/.oh-my-zsh/custom/
        done < "$filename"
}