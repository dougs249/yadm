#!/bin/zsh
#
#  Export Pacman / Yay list of programs
#  Installed: Explicit, dependencies, orphen, 
#  
#  Flatpak maybe

##  All packages
yay -Q | awk '{print $1}' '-'  > /home/doug/dotfiles/Packages/All-packages_yay-Q

##  Packages from Arch Repos (Core/Extra)
pacman -Qn | awk '{print $1}' '-' > /home/doug/dotfiles/Packages/Native-pacman-packages_Qn

##  Foreign packages. Packages installed from AUR or pkgbuild (IE: not from Pacman)
pacman -Qm | awk '{print $1}' '-' > /home/doug/dotfiles/Packages/Packages-from-AUR

##  Explicitly installed packages
yay -Qe | awk '{print $1}' '-'  > /home/doug/dotfiles/Packages/Explicit-packages_yay-Qe

##  Explicitly packages NOT needed by ANY other packages
yay -Qet | awk '{print $1}' '-'  > /home/doug/dotfiles/Packages/Explicit-AND-not-needed-by-any-others_yay-Qet

##  Orphaned packages
yay -Qdtq | awk '{print $1}' '-' > /home/doug/dotfiles/Packages/Orphaned-packages_yay-Qdtq

########
##  List all packages w/ size. Sort by size
pacman -Qi | awk '/^Name/{name=$3} /^Installed Size/{print $4$5, name}' | sort -h > /home/doug/dotfiles/Packages/Packages-by-size

########
##  Files NOT owned by any package (that pacman knows of)
# (export LC_ALL=C.UTF-8; comm -13 <(pacman -Qlq | sed 's,/$,,' | sort) <(find /etc /usr /opt -path /usr/lib/modules -prune -o -print | sort)) > /home/doug/dotfiles/Packages/Files-Unowned

########
##  Add to YADM and sync
# /home/doug/Code/shell-scripts/yadm-git/yadm-auto-sync.zsh

