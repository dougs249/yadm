#!/bin/bash

for zipfile in *.zip; do
  # Extract the filename without the .zip extension
  filename="${zipfile%.*}"
  
  # Create a directory with the filename
  mkdir -p "$filename"
  
  # Unzip the archive into the created directory
  unzip -q "$zipfile" -d "$filename"
  
  # Delete the original zip archive
  rm "$zipfile"
done
