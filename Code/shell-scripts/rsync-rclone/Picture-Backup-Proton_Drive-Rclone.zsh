#!/bin/zsh
# https://rclone.org/docs/
##  Back up Pictures to Proton Drive with Rclone
##    Do set up a inotify watch and trigger eventually 
##    OR
##    Just a Cron Job

# TEMPLATE
# Usage:
# rclone sync source:path dest:path [flags]
# command rclone sync  --progress -vv --fast-list --max-backlog=999999 --drive-chunk-size=512M  --buffer-size=75M  /home/doug/Porn/[xxxx]  protondrive:Porn/[xxxxx]

#### SEE  maan rclone  (for perrsonal man pages help)
# rclone config reconnect protondrive:
# rclone listremotes
# rclone lsf protondrive:
# rclone lsd protondrive:
# rclone authorize protondrive:
##############################################################
rclone md5sum source:<DIR> --output-file=<file>
rclone lsf -R --absolute --files-only source:<DIR> > <file>
#  See rclone lsf --help for other things to include (like hash, mod time, etc)
##############################################################
#############  OLD  ##########################################
# command rclone sync /mnt/Large-Files/Pictures/01-Rocco protondrive:Photos/01-Rocco/ --update --checksum --fix-case  --track-renames --progress -vv  --fast-list --ignore-errors --update --no-update-modtime --no-update-dir-modtime --check-first  --max-backlog=999999 --drive-chunk-size=512M  --buffer-size=75M 
##############################################################
rclone sync \
/home/doug/Pictures/01-Rocco \
protondrive:Photos/01-Rocco \
--ignore-errors --ignore-case --ignore-case-sync --ignore-checksum --ignore-existing \
--progress -vv --progress-terminal-title \
--update --fast-list --fix-case  --track-renames --refresh-times \
--modify-window=50ns \
--no-update-modtime --no-update-dir-modtime \
--streaming-upload-cutoff=100Ki \
--server-side-across-configs \
--size-only 
##############################################################
#                               Pictures [Regular]
# command rclone sync /mnt/Large-Files/Pictures \
# protondrive:Photos/ 
##############################################################
#                       Roccos pictures
command rclone sync /mnt/Large-Files/Pictures/01-Rocco \
protondrive:Photos/01-Rocco/ \

#                       Jets pictures
command rclone sync /mnt/Large-Files/Pictures/01-Jet \
protondrive:Photos/01-Jet/ \

rclone sync \
/mnt/Large-Files/Pictures/01-Jet \
protondrive:Photos/01-Jet \
--ignore-errors --ignore-case --ignore-case-sync --ignore-checksum --ignore-existing \
--progress -vv --progress-terminal-title \
--update --fast-list --fix-case  --track-renames --refresh-times \
--modify-window=50ns \
--no-update-modtime --no-update-dir-modtime \
--streaming-upload-cutoff=100Ki \
--server-side-across-configs \
--size-only

command rclone sync /mnt/Large-Files/Pictures/03-Me \
protondrive:Photos/03-Me \


command rclone sync /mnt/Large-Files/Pictures/04-Vacations \
protondrive:Photos/04-Vacations \


# command rclone sync /mnt/Large-Files/Pictures/05- \
# protondrive:Photos/05- \


command rclone sync /mnt/Large-Files/Pictures/06-Projects \
protondrive:Photos/06-Projects \


##############################################################
#                               Porn Folders
command rclone sync /home/doug/Porn \
protondrive:Porn/ \

##              Individual folders if giving trouble
# command rclone sync  /home/doug/Porn/00-sort  \
# protondrive:Porn/00-sort \

# command rclone sync  /home/doug/Porn/01-shota \
# protondrive:Porn/01-shota \

# command rclone sync /home/doug/Porn/02-Rule-34 \
# protondrive:Porn/02-Rule-34 \

# command rclone sync /home/doug/Porn/03-furry \
# protondrive:Porn/03-furry \

##############################################################
### Building better flags
##############################################################
--progress -vv \
--update --fast-list --fix-case  --track-renames --refresh-times --metadata \
--no-update-modtime --no-update-dir-modtime \
--ignore-errors  --ignore-case-sync --ignore-checksum --ignore-existing \
--transfers=15 --drive-chunk-size=512M  --buffer-size=75M \
--multi-thread-streams=4 --multi-thread-write-buffer-size=256Mi --multi-thread-cutoff=128Mi \
--max-duration=60m --max-backlog=1000 \
--separator="\n" \
--modify-window=50ns \
--streaming-upload-cutoff=100Ki \
--server-side-across-configs \
--delete-excluded \
--delete-during 

--order-by='size,ascending' --absolute \
--error=/home/doug/.log/rclone/errors.log \
--missing-on-dst=/home/doug/.log/rclone/missing-on-detstination \
--match=/home/doug/.log/rclone/matching-files \
--missing-on-src=/home/doug/.log/rclone/missing-on-source \
--dest-after=/home/doug/.log/rclone/post-sync-dest-files \
--differ=/home/doug/.log/rclone/all-nonmatching-files 

######
--size-only \
--ignore-case \
--check-first --checksum \
--interactive \
--dry-run
####
--max-age=2d \
####   all FLAGS
# --progress 
# -vv  
# --track-renames 
# --progress-terminal-title
# --transfers=N    
# -i, --interactive    
# -u, --update     Skip files that are newer on the destination
# --ignore-case-sync                            Ignore case when synchronizing
# --ignore-checksum                             Skip post copy check of checksums
# --ignore-existing                             Skip all files that exist on destination
# --check-first                                 Do all the checks before starting transfers
# -c, --checksum                                Check for changes with size & checksum (if available, or fallback to size only).

# --drive-chunk-size 64M (or 128M if you can afford up to 512M RAM total for rclone).
# --fast-list           Use recursive list if available; uses more memory but fewer transactions 
# --max-backlog=99999

# --multi-thread-chunk-size SizeSuffix          Chunk size for multi-thread downloads / uploads, if not set by filesystem (default 64Mi)
# --multi-thread-cutoff SizeSuffix              Use multi-thread downloads for files above this size (default 256Mi)
# --multi-thread-streams int                    Number of streams to use for multi-thread downloads (default 4)

# --multi-thread-write-buffer-size SizeSuffix   In memory buffer size for writing when in multi-thread mode (default 128Ki)
# --order-by string                             Instructions on how to order the transfers, e.g. 'size,descending'
# --max-backlog int                             Maximum number of objects in sync or check backlog (default 10000)
# --max-duration Duration                       Maximum duration rclone will transfer data for (default 0s)
# --max-transfer SizeSuffix                     Maximum size of data to transfer (default off)

# --fix-case                        Force rename of case insensitive dest to match source
# --refresh-times                               Refresh the modtime of remote files
# -M, --metadata                                    If set, preserve metadata when copying objects
# --immutable                                   Do not modify files, fail if existing files have been modified

# --delete-excluded                     Delete files on dest excluded from sync
# --exclude stringArray                 Exclude files matching pattern
# --exclude-from stringArray            Read file exclude patterns from file (use - to read from stdin)
# --exclude-if-present stringArray      Exclude directories if filename is present
# --files-from stringArray              Read list of source-file names from file (use - to read from stdin)
# --files-from-raw stringArray          Read list of source-file names from file without any processing of lines (use - to read from stdin)
# -f, --filter stringArray                  Add a file filtering rule
# --filter-from stringArray             Read file filtering patterns from a file (use - to read from stdin)

# --compare-dest stringArray                    Include additional comma separated server-side paths during comparison
# --copy-dest stringArray                       Implies --compare-dest but also copies files from paths into destination
# --cutoff-mode HARD|SOFT|CAUTIOUS              Mode to stop transfers when reaching the max transfer limit HARD|SOFT|CAUTIOUS (default HARD)

#####################
# logs n such
#######################
# --absolute                Put a leading / in front of path names
# -d, --dir-slash           Append a slash to directory names (default true)
# --combined string         Make a combined report of changes to this file
# --create-empty-src-dirs   Create empty source dirs on destination after sync
# --csv                     Output in CSV format
# -s, --separator string        Separator for the items in the format (default ";")
# -t, --timeformat string       Specify a custom tim (default: 2006-01-02 15:04:05)
# -F, --format string       Output format - see lsf help for details (default "p")

# --dirs-only               Only list directories
# --error string            Report all files with errors (hashing or reading) to this file
# --files-only              Only list files (default true)

# --hash h                  Use this hash when h is used in the format MD5|SHA-1|DropboxHash (default "md5")

# --dest-after string       Report all files that exist on the dest post-sync
# --differ string           Report all non-matching files to this file
# --match string            Report all matching files to this file
# --missing-on-dst string   Report all files missing from the destination to this file
# --missing-on-src string   Report all files missing from the source to this file

