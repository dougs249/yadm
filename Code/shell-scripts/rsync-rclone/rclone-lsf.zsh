#!/bin/zsh


rclone sync \
/home/doug/Pictures/01-Rocco/ \
protondrive:Photos/01-Rocco \
--progress -vv \
--update --fast-list --fix-case --track-renames --refresh-times --metadata \
--no-update-modtime --no-update-dir-modtime \
--ignore-errors  --ignore-case-sync --ignore-checksum --ignore-existing \
--transfers=15 --drive-chunk-size=512M  --buffer-size=75M \
--multi-thread-streams=4 --multi-thread-write-buffer-size=256Mi --multi-thread-cutoff=128Mi \
--max-duration=60m --max-backlog=1000 \
--separator="\n" \
--modify-window=50ns \
--streaming-upload-cutoff=100Ki \
--server-side-across-configs \
--delete-excluded \
--delete-during 

--order-by='size,ascending' --absolute \
--error=/home/doug/.log/rclone/errors.log 
--missing-on-dst=/home/doug/.log/rclone/missing-on-detstination \
--match=/home/doug/.log/rclone/matching-files \
--missing-on-src=/home/doug/.log/rclone/missing-on-source \
--dest-after=/home/doug/.log/rclone/post-sync-dest-files \
--differ=/home/doug/.log/rclone/all-nonmatching-files \



#####
# last working
#####
--progress -vv  \
--update --fast-list --fix-case  --track-renames --refresh-times \
--ignore-errors --ignore-case --ignore-case-sync --ignore-checksum --ignore-existing \
--transfers=15 --drive-chunk-size=512M  --buffer-size=75M \
--multi-thread-streams=4 --multi-thread-write-buffer-size=256Mi --multi-thread-cutoff=128Mi \
--max-duration=60m --max-backlog=1000 \
--order-by='size,ascending' --absolute \
--modify-window=50ns \
--no-update-modtime --no-update-dir-modtime \
--streaming-upload-cutoff=100Ki \
--server-side-across-configs \


yay -Slq | fzf --preview 'yay -Si {}' --height=90% --layout=reverse --preview-window=right,70% --bind 'enter:execute(yay -Si {} | less)'






