#!/bin/bash

# Will need to make a systemd timer maybe or set to run when Drive detected
rsync -r -n -t -p -v --progress --delete -u --partial -i -s /mnt/Large-Files/Games/Wii/01-Wii_Harddrive.bak/ /run/media/doug/NO_LABEL

# old
# rsync -r -n -t -p -v --progress -u -s /mnt/Large-Files/Games/Wii/01-Wii_Harddrive.bak/ /run/media/doug/NO_LABEL

### 
# Below will be templets for other things
###
# rsync -r -n -t -p -v --progress -u -s /mnt/Large-Files/Games/Wii/01-Wii_Harddrive.bak/ /run/media/doug/NO_LABEL