#!/bin/bash
#######
#
#  Call with 
#     ./git_update.sh ./gitrepos
#
#####
# path for file
# intakefilepath="./git-repo"
# echo "$intakefilepath"
###########################################

git-repo-locations () {
    rm ~/.gitupdate/r.log
    filename="$1"
    while IFS= read -r line; do
        # repo="$line"
        cd "${line%/*}"      # how to make a dir if not there
                    # query git and init if not repo, otherwise (if?)
        NOW=$( date '+%F_%H"%M:%S' )
        git pull -ff; git add *;git add .*; git stage --all; git commit -a -m "Auto Sync-- $NOW"; git push
        echo "${line}"
        # echo "Updated $line at $NOW." 
        # echo "###############################" 
        # echo "###############################" >> ~/.gitupdater.log
        # git-info 
        # echo "###############################"
        done < "$filename"
    # echo "###############################"
    # echo "see ~/.gitupdater.log for git-info output"
}
######################################
# trying to make it auto use ./git-repos path instead of having to type it
#     maybe load the list into an array and iterate over that??
#####################################
# git-repo-locations () {
#     filename="$1"
#     while IFS= read -r line; do
#         repo="$line"
#         cd "${line%/*}"      # how to make a dir if not there
#                     # query git and init if not repo, otherwise (if?)
#         NOW=$( date '+%F_%H"%M:%S' )
#         # git pull -ff; git add *;git add .*; git stage --all; git commit -a -m "Auto Sync-- $NOW"; git push
#         # echo "Updated $LINE at $NOW." >> ~/.gitupdater.log
#         pwd
#     done < "$filename"
# }

# git-repo-locations ($intakefilepath)
