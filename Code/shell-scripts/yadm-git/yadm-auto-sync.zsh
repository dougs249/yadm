#!/bin/zsh

cd /home/doug
yadm pull -ff

# Stuipid add ing all parent DIRS
# list with
# yadm -l
yadm add /home/doug/Code/shell-scripts/
yadm add .oh-my-zsh/custom/
yadm add .config/nvim/lua/
yadm add dotfiles/
yadm add .config/yadm/


# Commit changes
yadm commit -a -m "Auto Sync-- $NOW"

# Push
yadm push --progress


# 
