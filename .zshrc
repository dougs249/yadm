# ============= Init and MISC =============== {{{
# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
 source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi
# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
# 
export PATH=$PATH:/home/doug/.local/bin:/home/doug/.config/go/bin:/usr/local/go/bin:$HOME/bin:/usr/local/bin:/opt:/opt/android-sdk/platform-tools/:$PATH

export GOPATH=$HOME/.config/go
# ============ Start Zgenom ============= 
source "${HOME}/.zgenom/zgenom.zsh"
zgenom autoupdate
if ! zgenom saved; then
  echo "Creating a zgenom save"
  # specify plugins here
  zgenom ohmyzsh
  zgenom load romkatv/powerlevel10k powerlevel10k
  zgenom load joshskidmore/zsh-fzf-history-search   # load ctrl-r fzf history module
  zgenom load zshzoo/magic-enter    
  # new -----------
  zgenom load SleepyBag/zle-fzf
  zgenom load wfxr/formarks
  zgenom load pabloariasal/zfm
  # -----------
  zgenom load djui/alias-tips # https://github.com/djui/alias-tips
  zgenom load zsh-users/zsh-autosuggestions # https://github.com/zsh-users/zsh-autosuggestions/blob/master/INSTALL.md
  zgenom load Freed-Wu/zsh-colorize-functions
  zgenom load KulkarniKaustubh/fzf-dir-navigator
  zgenom load zdharma-continuum/fast-syntax-highlighting
  zgenom load wfxr/forgit
  zgenom load aoyama-val/zsh-delete-prompt # https://github.com/aoyama-val/zsh-delete-prompt
  zgenom load elstgav/branch-manager # https://github.com/elstgav/branch-manager
  # zgenom load MichaelAquilina/zsh-auto-notify # https://github.com/MichaelAquilina/zsh-auto-notify
  zgenom ohmyzsh plugins/magic-enter
  zgenom ohmyzsh plugins/zsh-navigation-tools
  zgenom ohmyzsh plugins/zsh-interactive-cd
  zgenom ohmyzsh plugins/colorize
  zgenom ohmyzsh plugins/tmux
  zgenom ohmyzsh plugins/colored-man-pages
  zgenom autoupdate
  zgenom save
fi
# }}}
# ============ Functions ============= 
# ======================
source ~/.oh-my-zsh/custom/functions.zsh
source ~/.oh-my-zsh/custom/shortcuts.zsh
source ~/.oh-my-zsh/custom/*
source ~/Code/shell-scripts/*
# ======================
# ======================
# fancy-ctrl-Z to ctrl-z out of and INTO vimrm
# ======================
fancy-ctrl-z () {
  if [[ $#BUFFER -eq 0 ]]; then
    BUFFER="fg"
    zle accept-line
  else
    zle push-input
    zle clear-screen
  fi
}
zle -N fancy-ctrl-z
bindkey '^Z' fancy-ctrl-z
#  ============ Keybinds (eventually) =============
bindkey '^t' delete-prompt
# ============ HELP and reminder WIP ============= 
# bindkey -M '^e' command fc
# ===============     buku  =================  
# https://buku.readthedocs.io/en/stable/README.html#quickstart
alias b='buku --suggest'
badd () {
  xsel | buku -a
}
zle -N badd
bindkey '^a' badd

# fuzzy search and open in firefox
alias fb='firefox $(buku -p -f 10 | fzf )'

# POSIX script to show a preview of the bookmark as well:
alias fbu='url=$(buku -p -f4 | fzf -m --reverse --preview "buku -p {1}" --preview-window=wrap | cut -f2)'
#if [ -n "$url" ]; then
#    echo "$url" | xargs firefox
#fi


#  EXAMPLES
# Edit and add a bookmark from editor:
# $ buku -w
# $ buku -w 'gedit -w'
# $ buku -w 'macvim -f' -a https://ddg.gg search engine, privacy



#fzf buku
#fb() {
      # save newline separated string into an array
      #     mapfile -t website <<< "$(buku -p -f 5 | column -ts$'\t' | fzf --multi)"
      #
      #         # open each website
      #             for i in "${website[@]}"; do
      #                     index="$(echo "$i" | awk '{print $1}')"
      #                             buku -p "$index"
      #                                     buku -o "$index"
      #                                         done
      #                                         })"
#}
#===================================================================  
# Aliases
#===================================================================
# yay -Yc 	Clean unneeded dependencies.
# **********      File Movement         *********** 
# alias grep="batgrep "
alias l="lsd "
alias ls="lsd "
alias ll="lsd -a "
alias lll="lsd -lah "
alias mkdir='mkdir -p '
alias cat="bat --style=plain --wrap=auto --force-colorization --paging=auto "
alias bat="bat --style=plain --wrap=auto --force-colorization --paging=auto "
alias du-interactive='sudo ncdu '
alias du-chart="duf -style ascii -theme ansi -output mountpoint,size,avail,usage,type,filesystem -width 200 -only local "
#alias du2='sudo du -cha --max-depth=2 | sort -h '
#alias du3='sudo du -cha --max-depth=3 | sort -h '
alias fdupes='fdupes -rdN '
#  for zsh DIR stack auto push ## https://thevaluable.dev/zsh-install-configure-mouseless/
alias d='dirs -v'
for index ({1..9}) alias "$index"="cd +${index}"; unset index
#  ***********    rsync and rclone    ***********   
alias rsync='rsync -avzP --inplace '
#       %*%*%*%*%        Git and Yadm       %*%*%*%*% 
alias gits="git status "
alias git-push='git push --progress '
alias gitup='git pull -ff; git add *;git add .*; git stage --all; git commit -a -m "Auto Sync-- $NOW"; git push --progress'
alias yadms='yadm status'
alias yadml='yadm list'
alias yadmup='sh /home/doug/Code/shell-scripts/yadm-git/yadm-auto-sync.zsh'
alias yadma='yadm add '
# **********        FZF and Search         ***********                
# alias fzf='fzf --preview "bat --color=always --style=numbers --line-range=:500 {}" --height=90% --layout=reverse --preview-window=right,55%'
# **********        Docker        ***********
alias dup="sudo docker compose up -d" 
alias ddn='sudo docker compose down'
alias dpull='sudo docker compose pull'
alias dps='sudo docker compose ps'
# **********       Global Aliases        *********** 
alias -g hh='--help | bat --language=Manpage '
     #  Pipe to grep
alias -g G=" | grep "
     #  Add Alias
alias -g ...=" ../.. "
# **********       System Maintnance        ***********  
alias ss="sync && sync"
alias sudo='sudo -v ; sudo '
alias sys-status='sudo systemctl status '
alias sys-start='sudo systemctl start '
alias sys-stop='sudo systemctl stop '
alias sys-restart='command sudo systemctl restart '
alias sys-mask='command sudo systemctl mask '
alias sys-enable='command sudo systemctl enable '
alias sys-reload='command sudo systemctl daemon-reload '
alias sys-edit='command sudo systemctl edit '
alias sys-cat='command sudo systemctl cat '
alias dracut-help='echo"sudo dracut --force --kver [kver in /usr/lib/modules/]"'
alias dracut-new='sudo dracut --force --kver  '
alias fstrim-my='sudo fstrim -v /; sudo fstrim -v /home'
# **********  Pacman and Yay (gen package management)  ***********
##  Cache and files
alias yS="yay -Syu  --combinedupgrade --save "
alias ys="yay -Ss "
alias news="eos-update --show-upstream-news"
alias install="sudo yay -Syu "
alias remove='sudo -v; yay -Rsn '
alias rebuild="yay -Syu --rebuild --answerclean all "
alias pac-cache-deps='sudo pacman -Qdtq | pacman -Rsn '
alias pac-cache-clean='sudo pacman -Scc'
alias pac-files="sudo pacman -Ql  "
alias pac-info="sudo pacman -Qi "
##  Update / etc
alias update="sudo -v; eos-update --yay; flatpak update --assumeyes; yay -Ps" #; sudo systemctl daemon-reload; echo 'CHECK-REBUILD'; checkrebuild"
alias yay-unattended="yay -Syu --noanswerclean --noanswerdiff --noansweredit --noanswerupgrade --norebuild --noconfirm "
# **********       Misc       ***********
alias top='sudo btop '
alias lsblk='lsblk -f '
alias lshw='sudo lshw '
alias xx="clear"
alias vim="nvim "
alias car='cat '
alias mount="sudo mount --mkdir  "
alias man='batman '
alias dd='dd status=progress '
alias exz="exec zsh "

# ===============     rsync WORKING  =================  

alias my-rsync-rm-src='rsync --remove-source-files  -avP --inplace '




alias gor='go run '
alias keyd=' sudo keyd '
